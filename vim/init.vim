" Bruce's Vim/Neovim Config

let mapleader = " "

"" Plugins:
"{{{
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

if has('nvim')
  call plug#begin('~/.config/nvim/plugged')
else
  call plug#begin('~/.vim/plugged')
endif
" Open files with fuzzy picker:
Plug 'srstevenson/vim-picker'
" Git:
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
" Surround text objects:
Plug 'tpope/vim-surround'
" Repeat plugin operations with '.':
Plug 'tpope/vim-repeat'
" Toggle comments
Plug 'tpope/vim-commentary'
" Case/plural-intelligent renaming
Plug 'tpope/vim-abolish'
" Function argument text objects:
Plug 'PeterRincker/vim-argumentative'
" Colorizing color literals
Plug 'lilydjwg/colorizer'
" Indent-wise movement
Plug 'jeetsukumaran/vim-indentwise'
" Indent code objects
Plug 'michaeljsmith/vim-indent-object'

" Repeat motions
Plug 'Houl/repmo-vim'
map <expr> ; repmo#LastKey(';')|sunmap ;
map <expr> , repmo#LastRevKey(',')|sunmap ,
" Repeat fFtT as usual (now with counts):
noremap <expr> f repmo#ZapKey('f')|sunmap f
noremap <expr> F repmo#ZapKey('F')|sunmap F
noremap <expr> t repmo#ZapKey('t')|sunmap t
noremap <expr> T repmo#ZapKey('T')|sunmap T
" Now following can also be repeated with `;` and `,`:
for keys in split("[[:]] []:][ [m:]m [M:]M {:} (:) w:b W:B e:ge E:gE")
    let [key1, key2] = split(keys, ":")
    execute 'noremap <expr> '.key1." repmo#SelfKey('".key1."', '".key2."')|sunmap ".key1
    execute 'noremap <expr> '.key2." repmo#SelfKey('".key2."', '".key1."')|sunmap ".key2
endfor
nmap <expr> ]c repmo#Key('<Plug>(GitGutterNextHunk)', '<Plug>(GitGutterPrevHunk)')
nmap <expr> [c repmo#Key('<Plug>(GitGutterPrevHunk)', '<Plug>(GitGutterNextHunk)')

""" Language Plugins:
"{{{
Plug 'dag/vim-fish'
Plug 'https://bitbucket.org/spilt/vim-nomsu'
Plug 'https://bitbucket.org/spilt/vim-peg'
Plug '~/software/vim-bpeg'
Plug '~/software/vim-sss'
Plug 'kballard/vim-swift'
Plug 'leafo/moonscript-vim'
Plug 'pangloss/vim-javascript'
Plug 'wlangstroth/vim-racket'
Plug 'gabrielelana/vim-markdown'
Plug 'lervag/vimtex'
Plug 'StanAngeloff/php.vim'
Plug 'perillo/qbe.vim'
Plug 'fsharp/vim-fsharp', {'for': 'fsharp', 'do': 'make fsautocomplete'}
Plug 'rust-lang/rust.vim'
Plug 'jcorbin/vim-lobster'

let g:rustfmt_autosave = 1
let g:rustfmt_emit_files = 1
let g:rustfmt_fail_silently = 0
"}}}

call plug#end()

""" Vimtex Config:
"{{{
let g:tex_flavor='latex'
let g:vimtex_view_method='zathura'
"}}}

""" Vim-Picker Config:
"{{{
let g:picker_height = 20
let g:picker_custom_find_executable = "find"
let g:picker_custom_find_flags = ". -type f -printf '%P\n'"
noremap <silent> <leader>l :PickerBuffer<CR>
noremap <silent> <leader>b :PickerEdit<CR>
noremap <silent> <leader>f :PickerEdit<CR>
"}}}

"}}}

"" Vim Behavior:
"{{{
syntax on
set nocompatible

" Buffers
set hidden

"For using sys clipboard
set clipboard=unnamedplus

" Use 'man' for keyword lookup
runtime ftplugin/man.vim
set keywordprg=:Man\ 2

"disable the .swp files
set noswapfile

"enable undo log
set undofile
if has('nvim')
  set undodir=~/.local/share/nvim/undo
else
  set undodir=~/.vim/undo
endif

"auto-reload files
set autoread
au FocusGained * :checktime
if has('nvim')
  au VimResume * :checktime
endif

""" Search Behavior:
"{{{
"highlight search results
set hlsearch incsearch
if has('nvim')
  set inccommand=split
endif
set ignorecase smartcase
"}}}

"" Line Wrapping:
"{{{
" By default, don't wrap at all
set nowrap
" Allow cursor to go beyond the last character
set virtualedit=onemore
" Use line wrapping for cursor movement
set whichwrap+=<,>,h,l,[,]
set linebreak
set backspace=2
set scrolloff=6
" Single space after period.
set nojoinspaces
"}}}

"" Indentation:
"{{{
"indentation crap
filetype indent on
set autoindent
set tabstop=4
set shiftwidth=0
set expandtab

" Sane C-indentation options
set cinoptions=L2,l1,(0,W4,m1

""" Auto-detect Indentation:
"{{{
if !exists("*TabsOrSpaces") 
  function TabsOrSpaces()
    let numTabs=len(filter(getbufline(bufname("%"), 1, 1024), 'v:val =~ "^\\t"'))
    let numSpaces=len(filter(getbufline(bufname("%"), 1, 1024), 'v:val =~ "^ "'))
    if numTabs > numSpaces
      setlocal noexpandtab
    endif
    if len(filter(getbufline(bufname("%"), 1, 1024), 'v:val =~ "^  [^ ]"')) > 0
      setlocal ts=2
    endif
  endfunction
endif

" Call the function after opening a buffer
autocmd BufReadPost * call TabsOrSpaces()
"}}}
"}}}

"Splits
set splitright splitbelow

set diffopt=filler,vertical,context:9999
if has('nvim')
  set diffopt+=internal
endif

set laststatus=2
"}}}

"" Display Configuration:
"{{{
"Set window title:
set title titlestring=Vim:\ \ \ %{expand(\"%:.\")}%m\ \ \ (%{substitute(getcwd(),$HOME,\"~\",\"\")}/)

set statusline=%f\ %h%w%m%r\ %=%(%l,%c\ [%o]\ %=\ %P%)

if $TERM_PROGRAM =~ "iTerm"
  let &t_SI = "\<Esc>]1337;CursorShape=1\x7" " Vertical bar in insert mode
  let &t_EI = "\<Esc>]1337;CursorShape=0\x7" " Block in normal mode
else
  " blinking vertical bar
  let &t_SI .= "\<Esc>[5 q"
  " solid block
  let &t_VS .= "\<Esc>[2 q"
  let &t_EI .= "\<Esc>[2 q"
  " solid underscore
  let &t_SR .= "\<Esc>[4 q\<Esc>]12;red\x7"
  " 1 or 0 -> blinking block
  " 3 -> blinking underscore
  " Recent versions of xterm (282 or above) also support
  " 5 -> blinking vertical bar
  " 6 -> solid vertical bar
endif

"line numbers
set number

""" Colors:
"{{{
set t_Co=256
set background=dark

hi StatusLine cterm=none ctermbg=237 ctermfg=gray
hi Pmenu ctermbg=DarkGray guibg=gray

hi LineNr ctermfg=brown

" vimdiff
hi DiffAdd    cterm=none ctermfg=NONE ctermbg=22
hi DiffChange cterm=none ctermfg=NONE ctermbg=18
hi DiffDelete cterm=bold ctermfg=NONE ctermbg=52
hi DiffText   cterm=none ctermfg=NONE ctermbg=54

hi Todo ctermbg=166

hi MarkdownCodeBlock ctermfg=gray
hi MarkdownCodeDelimiter ctermfg=darkgray
hi MarkdownCode ctermbg=black ctermfg=white
hi MarkdownBlockquote ctermfg=darkblue

"tweak fold highlighting
hi Folded ctermfg=darkgray ctermbg=none

hi MatchParen cterm=bold ctermbg=NONE ctermfg=blue
"}}}
"}}}

"" Key Bindings:
"{{{
set mouse=a

inoremap jj <ESC>
inoremap jJ <ESC>
inoremap Jj <ESC>

noremap <leader>q :confirm quitall<CR>

" when repeating an action, don't move the cursor
nnoremap . .`[

" Sensible yank:
noremap Y y$

""" Pasting:
"{{{
" delete without yanking
nnoremap dd "ddd
noremap d "dd
noremap D "dD
" Only delete without yanking with x in normal mode:
nnoremap x "dx

" replace currently selected text with default register
" without yanking it
vnoremap p "ddP
vnoremap P "ddP

nnoremap H ^
nnoremap L g$

" stop pasting when leaving insert mode
autocmd InsertLeave * set nopaste
"}}}

""" Movement:
"{{{
"Move by 1 word at a time in insert mode
inoremap <c-w> <s-right>
inoremap <c-b> <s-left>
inoremap <c-a> <c-o>$

"move by display lines
noremap k gk
noremap j gj
noremap gk k
noremap gj j

"shifting lines up and down in visual mode with J/K
vnoremap <silent> <s-J> :m '>+1<CR>gv
vnoremap <silent> <s-K> :m '<-2<CR>gv
"}}}

"insert newlines without entering insert mode
nnoremap <expr> <CR> &buftype =~# "^quickfix\\\|help$" ? "\<CR>" : "o\<Esc>"
nnoremap <expr> g<CR> &buftype =~# "^quickfix\\\|help$" ? "\<CR>" : "O\<Esc>"

" Select last modified range
nnoremap gz `[v`]

" CR to leave visual mode
vnoremap <CR> <Esc>

" Search text selection
vnoremap * "zy:let @/=@z<CR>n

"Toggle escaping
"nnoremap <silent> <Bslash> i\<Esc>l
"vnoremap <silent> <Bslash> :s/\%V\\/\\\\/ge<CR>gv:s/\%V"/\\"/ge<CR>:noh<CR>gv
"vnoremap <silent> <Bar> :s/\%V\\\(.\)/\1/ge<CR>:noh<CR>gv

" run system commands *inline* with leader+r
nnoremap <expr> <silent> <leader>r 'i<c-r>=trim(system("'.escape(input('> '),'"\').'"))<CR><Esc>'
vnoremap <leader>r c<c-r>=trim(system(escape(getreg('"'), '"\')))<CR><Esc>

""" Indentation:
"{{{
vnoremap > >gv
vnoremap < <gv

" Backwards Ctrl-O
noremap g<c-o> <c-i>
"}}}

" Syntax highlighting debugging
noremap <leader>h :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>
noremap <leader>sr :syn off<CR>:syn on<CR>:echo "Syntax reloaded."<CR>

" Split
noremap <silent> <leader>s :50vne<CR>

" Command line tools
noremap <silent> <leader>a :47vne +term\ ascii<CR>
noremap <silent> <leader>c :82vne +term\ ansi<CR>
noremap <silent> <leader>m :silent make<CR>:echo "made."<CR>
nnoremap <silent> <leader>C "cciw<c-r>=trim(system("pickcolor '".getreg("c")."'"))<CR><Esc>
vnoremap <silent> <leader>C "cc<c-r>=trim(system("pickcolor '".getreg("c")."'"))<CR><Esc>

nnoremap <silent> <leader>i mi:let @+=system("c-imports ".expand("<cword>"))<CR>gg/#include<CR>:noh<CR>PV}k:sort u<CR>`izz

"}}}

"" Autocompletion:
"{{{
set completeopt=menu,preview
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType html setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
autocmd FileType php setlocal omnifunc=phpcomplete#CompletePHP
autocmd FileType c setlocal omnifunc=ccomplete#Complete commentstring=//\ %s
autocmd FileType text,md setlocal wrap
autocmd FileType rust,rs map <silent> <leader>r :Cargo run<CR>
"}}}

"" File Associations/Autocmds:
"{{{
filetype on
filetype plugin on
autocmd FileType * setlocal formatoptions-=cro
autocmd BufNewFile,BufRead *.S setlocal filetype=asm ts=5
autocmd BufNewFile,BufRead *.s setlocal filetype=asm ts=5
autocmd BufNewFile,BufRead *.h setlocal filetype=c
autocmd BufNewFile,BufRead *.glsl setlocal filetype=glsl syntax=c
autocmd BufNewFile,BufRead *.rkt setlocal filetype=rkt syntax=lisp
autocmd BufNewFile,BufRead *.fish setlocal filetype=fish syntax=fish
autocmd Filetype css setlocal tabstop=2
autocmd Filetype html setlocal tabstop=2
autocmd Filetype html syntax sync fromstart
autocmd Filetype js setlocal tabstop=2
autocmd BufReadCmd *.love call zip#Browse(expand("<amatch>"))
autocmd BufWritePost ~/scripts/* silent !chmod +x %
autocmd QuickFixCmdPost * :cwindow 5
if has('nvim')
  autocmd TermOpen * startinsert
endif

""" Spellcheck:
"{{{
set spelllang=en_us
autocmd Filetype markdown setlocal spell
autocmd Filetype text setlocal spell
autocmd FileType help setlocal nospell
hi SpellBad   cterm=none ctermbg=none ctermfg=211
hi SpellCap   cterm=none ctermbg=none ctermfg=189
hi SpellRare  cterm=none ctermbg=none ctermfg=193
hi SpellLocal cterm=none ctermbg=none ctermfg=222
"}}}
"}}}

"" Netrw Config:
"{{{
let g:netrw_banner=0
let g:netrw_liststyle=3
let g:netrw_list_hide=netrw_gitignore#Hide()
"file browsing ignores
let g:netrw_list_hide = '\.swp$,\.jpg$,\.exe$,\.pyc$,\.jpeg$,\.png$,\.gif$,\.wav$,\.mp3$,\.o$,\.obj$,\.so$,\.pdf$,\~$'
let g:netrw_sort_sequence = '[\/]$,\.bak$'
let g:netrw_sort_options = "i"
let g:netrw_keepdir = 0
"}}}

" vim: filetype=vim foldmethod=marker foldlevel=0
