-- Clipshot - copy screenshot to clipboard
local script = [=[
convert /tmp/mpv-screenshot.png -resize "800x800>" - | xclip -selection clipboard -t image/png;
rm /tmp/mpv-screenshot.png
]=]
function clipshot()
    mp.commandv('screenshot-to-file', '/tmp/mpv-screenshot.png', 'subtitles')
    mp.command_native_async({'run', '/bin/sh', '-c', script}, function(suc, _, err)
        mp.osd_message(suc and 'Copied screenshot to clipboard' or err)
    end)
end

mp.add_key_binding('c', 'clipshot', clipshot)
