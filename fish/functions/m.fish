function m
    if test -z $argv[1]
        set name (ask -q "Create mark: ") || return
    else
        set name $argv[1]
    end
    if test -e ~/.config/jumpmarks/"$name"
        rm ~/.config/jumpmarks/"$name"
    end
    ln -sf (pwd) ~/.config/jumpmarks/"$name"
end

