function vipe
    set -l f (mktemp)
    cat > $f
    eval $EDITOR $f < (tty) > (tty)
    cat $f
    rm $f
end

