function license --description 'Print a software license'
    cat /usr/local/share/licenses/$argv \
    | sed -e "s/<year>/"(date "+%Y")"/" \
          -e "s/<name>/Bruce Hill/" \
          -e "s/<email>/bruce@bruce-hill.com/"
end

