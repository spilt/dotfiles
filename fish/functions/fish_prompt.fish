function fish_prompt
    set prev_status $status
    printf '\033[J'
    if test $prev_status -eq 0
        set_color green; printf "ᕕ(ᐛ)ᕗ "
    else
        set_color red; printf "(ಥ_ಥ) "
    end
    if [ -n "$RANGER_LEVEL" ]
        set_color black -b red; printf " R$RANGER_LEVEL "
    end
    if [ -n "$BB_DEPTH" ]
        set_color black -b red; printf " BB$BB_DEPTH "
    end

    #set_color 'e0a735' -b normal; printf " "
    #set_color black -b 'e0a735'; printf " $USER "

    #set_color blue -b normal; printf " "
    set_color black -b 'e0a735'; printf " "(pwd | perl -pe "s!^$HOME!~!; s!([^/0-9.]{4})[^/0-9.]{2,}(?=.*/)!\1…!g")" "

    set gitprompt (__fish_git_prompt ' %s')
    if [ $gitprompt ]
        #set_color green -b normal; printf " "
        set_color black -b blue
        printf " $gitprompt "
    end

    for job in (jobs | awk '{print $1".\x1b[3m"$5"\x1b[23m"}' | sort)
        printf "\033[0m\033[38;5;240m-\033[30m\033[48;5;240m $job "
        #set_color magenta -b normal; printf "-"
        #set_color black -b magenta; printf " $job "
    end

    printf "\033[0m\n\033[2mf\033[0m "
    set_color green -b normal
    printf "╰─▶ \033[0m"
end
