function fzy_history
    if test (count $argv) = 0
        set flags
    else
        set flags -q "$argv"
    end
    commandline -r (history | fzf $flags || echo "")
end
