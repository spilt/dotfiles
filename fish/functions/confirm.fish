function confirm
    while true
        read -l -P "$argv [Y/n] " confirmation
        switch $confirmation
          case '' Y y
            return 0
          case N n
            return 1
        end
    end
end
