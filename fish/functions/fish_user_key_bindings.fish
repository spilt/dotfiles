# Defined in /tmp/fish.gewkTO/fish_user_key_bindings.fish @ line 21
function fish_user_key_bindings
	bind \cr 'fzy_history (commandline -b)'
    #bind -s --user -M default H backward-word
    #bind -s --user -M visual H backward-word
    #bind -s --user -M default L forward-word
    #bind -s --user -M visual L forward-word
    bind ! bind_bang
    bind '$' bind_dollar
    bind \cB 'bcd; commandline -f repaint'
    bind \cF 'fg 2>/dev/null; commandline -f repaint'
    bind \e\cB 'commandline -i (bb -s | tr "\n" " ")'
end
