function j
    set -lx mark (ls ~/.config/jumpmarks | fzf -1 --height=5 -q "$argv")
    and cd (readlink -f ~/.config/jumpmarks/"$mark")
end

